import { IContactUser } from './contact';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Contacts provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ContactProvider {

  private users : IContactUser[]  = [{
    name : "Jasper de Jesus",
    phoneNumber : "09176903189",
    avatar : "assets/images/image-1.jpg"
  },{
    name : "Louisse Morales",
    phoneNumber : "09151991797",
    avatar : "assets/images/image-4.jpg"
  }]

  constructor(public http: Http) {
  }

  getUsers() : IContactUser[] {
    return this.users;
  }

  addUser(user : IContactUser){
    this.users.push(user)
  }


}


export interface IContactUser{
  name : string,
  phoneNumber : string,
  avatar : string,
}
