import { IContactUser, ContactProvider } from './../../providers/contact';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the AddContact page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-contact',
  templateUrl: 'add-contact.html'
})
export class AddContactPage {

  private avatars = [
    "assets/images/image-1.jpg",
    "assets/images/image-2.jpg",
    "assets/images/image-3.jpg",
    "assets/images/image-4.jpg",
    "assets/images/image-6.jpg",
    "assets/images/image-7.jpg",
    "assets/images/image-8.jpg",
    "assets/images/image-9.jpg"
  ]

  contact: IContactUser = {
    name : "",
    phoneNumber : "",
    avatar : ""
  };

  constructor(public navCtrl: NavController, public contactProvider : ContactProvider) {}

  selectAvatar(avatar){
    this.contact.avatar = avatar;
  }

  addContact(){
    this.contactProvider.addUser(this.contact)
    this.navCtrl.pop()
  }

}
