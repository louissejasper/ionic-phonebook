import { ViewContactPage } from './../view-contact/view-contact';
import { AddContactPage } from './../add-contact/add-contact';

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { ContactProvider, IContactUser } from '../../providers/contact'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contacts : IContactUser[] = []

  constructor(public navCtrl: NavController, private contactProvider : ContactProvider) {
    this.contacts = this.contactProvider.getUsers()
  }

  search(text:string){

  }

  goToAddPage(){
    this.navCtrl.push(AddContactPage)
  }

  test($event:any){
    $event.stopPropagation()
  }

  testSelect(){
  }
  
  testItem(){
  }

  viewContact(){
    this.navCtrl.push(ViewContactPage)
  }

}
