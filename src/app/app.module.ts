
import { ViewContactPage } from './../pages/view-contact/view-contact';
import { AddContactPage } from './../pages/add-contact/add-contact';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';


import { ContactProvider } from '../providers/contact'

@NgModule({
  declarations: [
    MyApp,
    AddContactPage,
    HomePage,
    ViewContactPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddContactPage,
    ViewContactPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactProvider,
  ]
})
export class AppModule {}
